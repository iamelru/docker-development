NodeJS Typescript boilerplate

## IDE Setup ##
In the project root dir are two files:

* tslint.json
* .editorconfig

Whatever IDE you are using, let's please install any plugins necessary to work with those so the repo stays consistant

* for vscode: "vscode-tslint" and "EditorConfig for Visual Studio Code"
* for Sublime: "SublimeLinter" with "SublimeLinter-contrib-tslint" and "EditorConfig"

VSCode has some nifty extensions for Docker and CloudFormation support. Extension Names are:

* Docker
* CloudFormation

## Docker ##

### Docker Prerequisites ###

1. Install [Docker](http://docker.io/)

### Docker Quickstart ###

1. Build Container

```shell
docker build -f Dockerfile -t hls-packager-service:latest .
```

* The -f flag allows you to specify the Dockerfile to use
* The -t flag tags the docker image with the specified name

2. Your image should now be listed by Docker

```shell
$ docker images
REPOSITORY             TAG                 IMAGE ID            CREATED             SIZE
hls-packager-service   latest              80ed8834573c        11 minutes ago      825 MB
node                   latest              6c792d919591        4 days ago          666 MB
```

3. Run the image using docker-compose specifying debug or production yaml file. You may need to save docker-compose.yml as docker-compose.dev.yml to make your own local yaml for your local environment.

```shell
docker-compose -f docker-compose.dev.yml up
```

4. You should now be able to see your running process

```shell
$ docker ps
CONTAINER ID        IMAGE                         COMMAND                  CREATED             STATUS              PORTS               NAMES
0d7cc9743993        hls-packager-service:latest   "/bin/sh -c 'npm s..."   6 minutes ago       Up 6 minutes        3000/tcp            upbeat_payne
```

5. To stop the image using docker-compose specifying debug or production yaml file

```shell
docker-compose -f docker-compose.dev.yml down
```
