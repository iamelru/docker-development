class ExampleClass {
    constructor() { }

    public test() {
        return new Promise((resolve, reject) => {
            if (Math.random() > .999) {
                resolve(true)
            } else {
                // console.log(.name)
                reject(`${this.constructor.name}\'s method test() rejected`)
            }
        })
    }

    public to() {
        return new Promise(resolve => {
            setTimeout(() => resolve(2), 1000)
        })
    }
}

export const my = new ExampleClass();