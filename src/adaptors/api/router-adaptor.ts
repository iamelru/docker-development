import { Router } from 'express'
export abstract class RouterAdaptor {
    public router: Router
    // Initialize the router and its routes
    constructor() {
        this.router = Router()
        this.initRoutes()
    }
    public abstract initRoutes()
}
