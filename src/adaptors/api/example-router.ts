import { RouterAdaptor } from './router-adaptor'
import { Request, Response, NextFunction } from 'express'

class ExampleRouter extends RouterAdaptor {
    constructor(){
        super()
    }

    /**
     * initRoutes
     */
    public initRoutes() {
        this.router.get('/', this.exampleHandler.bind(this))
        this.router.get('/page', this.page.bind(this))
    }

    private async exampleHandler(req: Request, res: Response, next: NextFunction) {
        try {
            this.handleResponse(200, {your: "data"}, res)
        } catch (error) {
            this.handleResponse(404, {your: "data"}, res)
        }
    }

    private async page(req: Request, res: Response, next: NextFunction) {
        try {
            res.render('./example/index', {message: "jopa"} )
        } catch (error) {
            this.handleResponse(404, {
                message: error.message,
                success: false
            }, res)
            return
        }
    }


    /**
     *
     * @private
     * @param {number} status
     * @param {any} data
     * @param {Response} res
     *
     * @memberof ExampleRouter
     */
    private handleResponse(status: number, data: any, res: Response) {
        res.status(status).send(data)
    }
}

export default new ExampleRouter().router