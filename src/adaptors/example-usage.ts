import { my } from './example-class'

async function test() {
    try {
        const x = await my.test()
        const y = await my.to()
        console.log(x, y)
    } catch (err) {
        console.log(err)
    }
}