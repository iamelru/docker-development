// Global rejections handler
let unhandledRejections = new Map();

process.on('unhandledRejection', (reason,promise) => {
    console.log('[Unhadled rejections]:', reason)
    unhandledRejections.set(promise, reason)
});

process.on('rejectionHandled', promise=>{
    unhandledRejections.delete(promise)
})

function shutdown(signal, value) {
    process.exit(128 + value)
}

setInterval(()=>{
    if(unhandledRejections.size) console.log('[Unhadled rejections] Got some rejections:')
    unhandledRejections.forEach((reason, promise)=>{
        promise.catch( (err: any) => console.log('--', err.message ? err.message: err, promise))
    })
    unhandledRejections.clear()
}, 60000)
// End of global rejections handler
