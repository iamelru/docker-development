import * as express from 'express'
import * as bodyParser from 'body-parser'


import exampleRouter from './adaptors/api/example-router'

class App {

    // ref to Express instance
    public express = express()

    // Run configuration methods on the Express instance.
    constructor() {
        this.middleware()
        this.views()
        this.routes()
      
    }

    // Configure Express middleware.
    private middleware() {
        this.express.use(bodyParser.json())
        this.express.use(bodyParser.urlencoded({
            extended: false
        }))
    }

    private routes() {
        const router:express.Router = express.Router()

        router.get('/', (req, res, next) => {
            res.json({
                message: 'Hello, world!!!'
            })
        })

        this.express.use('/', router)


        // Routes into application
        this.express.use('/example', exampleRouter)
    }

    // Set up a template view engine 
    private views() {
        this.express.set('view engine', 'pug')
    }
}

const app = new App().express
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Example app listening on port ${port}!`))
