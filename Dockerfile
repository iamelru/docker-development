FROM mhart/alpine-node:10

WORKDIR /usr/app

COPY ["package.json", "package-lock.json", "./"]

RUN npm i typescript -g && \
    npm i --quiet 

COPY . .